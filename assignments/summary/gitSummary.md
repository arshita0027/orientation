### **Git Summary**
#### **1.Basics**

 In simple words, Git is a version control software that makes collaboration on large projects simple.  
 Advantages that come with git are-  
 - Coordinating work among team members.  
 - Track changes in set of files.  
 - Speed and data integrity for non-linear workflows  


#### **2.Git vocabulary**

 - Repository- Collection of files and folders  
 - Gitlab- Second most popular remote storage for git repos. (First being github)  
 - Commit- Saving your work is same as commit to repo. They exist only on local machine  
 - Push- Syncing your commits to gitlab.  
 - Branch- Main software is the main branch of the tree. The seperate instances of code other than main codebase are called branches.  
 - Merge- When a branch is free of bugs, it is integrated with the main branch.This is called merging.  
 - Clone- Cloning takes the entire repo and creates a copy of it on local machine.  
 - Fork- Similar to cloning, forking duplicates the existing repo on your local machine, under your name.

#### **3.Installation**  
 Git can be installed on Linux, Mac or Windows.For our project, to install git on Ubuntu  
`sudo apt-get install git`  

#### **4. Internals**  

- Files can reside in Three main states:  
 1. *Modified*- Changed the file but have not committed it to your repo yet.  
 2. *Staged*- Marked a modified file in its current version to go into your next picture/snapshot.  
 3. *Committed*- The data is safely stored in your local repo in form of pictures/snapshots.  

- There are 3-4 different trees of repository present at a time:  
 1. *Workspace*- All the changes you make via Editor are stored in this repo  
 2. *Staging*- All the staged files go into this tree of your repo  
 3. *Local Repository*- All the committed files go to this tree of your repo  
 4. *Remote Repository*- This is a version of Local Repository stored in some server on the Internet.   
     Changes in the local repo should be seperately pushed into remote repo  
![](https://www.vippng.com/png/detail/395-3954416_git-workflow-git-commit-workflow.png)


#### **5. Workflow** 

Steps:
1. Clone the repo and create a new branch-  
 ` git clone <link-to-repository>`  
 ` git checkout master`  
 ` git checkout -b <your-branch-name>`    
2. Modify files in your working tree  
  for ex-  `git add`  
3. Commit your changes that permanently stores changes in the local repository  
  `git commit`  
4. (Additional step) Update the local repo from remote repo using pull command 
 `git pull`  
 [Git Pull documentation](https://git-scm.com/docs/git-pull)
5. Push the files from local repo to remote repo  
 ` git push origin <branch-name>`  
![](https://www.tangowithdjango.com/book/_images/git-sequence.svg)